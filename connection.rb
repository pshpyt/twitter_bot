require 'rubygems'
require 'sqlite3'
require 'active_record'

#ActiveRecord::Base.logger = Logger.new(File.open('database.log', 'w'))

ActiveRecord::Base.establish_connection(
  :adapter   => 'sqlite3',
  :database  => './data.db'
)

ActiveRecord::Schema.define do
  unless ActiveRecord::Base.connection.tables.include? 'accounts'
    create_table :accounts do |table|
      table.column :username, :string 
      table.column :twitter_id, :string , null: false
      table.column :status, :string 
      table.column :send_request_at, :date
     
    end
  end


end

class Account < ActiveRecord::Base
  validates :twitter_id, uniqueness: true
  scope :fresh, -> { where(:status => 'new') }
  scope :awaiting, -> { where(:status => 'followed').where(["send_request_at < ?", 1.day.ago]) }
end




