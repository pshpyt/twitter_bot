require 'rubygems'
require 'bundler/setup'
require 'twitter'
require 'json'
require 'yaml'
require_relative 'connection.rb'
 
class Bot
  def initialize()
    @options = YAML.load_file('config.yml')
    @source_user = @options["source_user"]
  end
      
  # Create the Twitter REST client instance
  def client
    @client ||= ::Twitter::REST::Client.new do |config|
      config.consumer_key        = @options["consumer_key"]
      config.consumer_secret     = @options["consumer_secret"]
      config.access_token        = @options["access_token"]
      config.access_token_secret = @options["access_token_secret"]
    end
  end

  # Run the bot action
  def run
    log("Starting bot ...")
    if Account.fresh.size < 1000
      log("Add more twitter ids to db ...")
      populate_db
    end
    run_unfollower
    run_follower
  end
    
  def run_follower
    log("Start follow next group of twitter users ...")
    Account.fresh.all[0..@options["step"].to_i].each do |user|
      begin
        rate_limited_follow(user.twitter_id.to_i)
        user.update(:status=>"followed", :send_request_at => Date.today)
        log("#{user.twitter_id} ... #{user.status}")
      rescue Exception => e  
        log(e.message)
        log(e.backtrace.inspect)  
      end
    end
  end
  
  def rate_limited_follow(user_id)
    num_attempts = 0
    begin
      num_attempts += 1
      client.follow(user_id)
    rescue Twitter::Error::TooManyRequests => error
      if num_attempts % 3 == 0
        puts "--> sleep for 15 minutes"
        sleep(15*60) # minutes * 60 seconds
        retry
      else
        retry
      end
    end
  end
  
  def rate_limited_unfollow(user_id)
    num_attempts = 0
    begin
      num_attempts += 1
      client.unfollow(user_id)
    rescue Twitter::Error::TooManyRequests => error
      if num_attempts % 3 == 0
        puts "--> sleep for 15 minutes"
        sleep(15*60) # minutes * 60 seconds
        retry
      else
        retry
      end
    end
  end

  def run_unfollower
    # find gilty users
    guilty_from_db.each do |user_id|
      #client.unfollow(u.to_i)
      begin
        rate_limited_unfollow(user_id)
        account = Account.where(:twitter_id => user_id.to_s).first
        account.update(:status => "unfollowed")
        log("#{account.twitter_id} ... unfollowed")
      rescue Exception => e  
        log(e.message)
       # log(e.backtrace.inspect)  
      end
    end      
    #unfollow gilty users   
  end  
  
  # populate db with twitter users
  def populate_db
    target_followers.each do |id|
      account = Account.create(:twitter_id => id, :status => "new" )
      log("#{account.twitter_id} --> #{account.status}")
    end 
  end
  
    # get followers for slected user
  def target_followers
    client.follower_ids(@source_user).attrs[:ids]
  end
 
  # get next 5000 twitter users
  def get_follower_ids
    follower_ids = []
    next_cursor = get_next_cursor
    cursor = client.follower_ids(:screen_name=> @options["source_user"], :cursor => next_cursor)
    follower_ids.concat cursor.attrs[:ids]
    next_cursor = cursor.attrs[:next_cursor]
    save_cursor(next_cursor)
    follower_ids
  end
  
  def get_next_cursor
    temp_data = YAML.load_file('temp.yml')
    temp_data[:next_cursor]
  rescue
    -1
  end 
  
  def save_cursor(next_cursor)
    temp_data = {:next_cursor => next_cursor}
    File.open("temp.yml", "w") do |file|
      file.write temp_data.to_yaml
    end
  end

  #Returns a array IDs for every protected 
  #user for whom the authenticating user has a pending follow request.
  def pending_requests
    client.friendships_outgoing.attrs[:id]
  end
  # my guilty guys
  def my_guilty
    guilty(client.user.id)
  end
  # users which do not follow back 
  def guilty(user_id)
    (client.friend_ids(user_id).attrs[:ids] - client.follower_ids(user_id).attrs[:ids])
  end
  
  #arr of user's ids from db which do not follow back
  def guilty_from_db
    (awaiting_arr & my_guilty)
  end 
  
  # arr of ids for users with awaiting status
  def awaiting_arr
    Account.awaiting.map{ |u| u.twitter_id.to_i}
  end
  
  def log(string)
    puts "#{Time.now.strftime("%I:%M:%S")}: #{string}"
  end
end
  
  bot = Bot.new()
  bot.run
 
